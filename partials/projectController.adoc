
- *Main endpoints*

[cols="3",options="header"]
|=======================
|#|Endpoint      |Verb
|1|`/Project`  |*POST*
|2|`/Project`  |*DELETE*
|3|`/Project/Project/:projectId/member`  |*POST*
|4|`/Project/Project/:projectId/member/Project`  |*DELETE*
|=======================

image::../partials/diagrams/workflow1.svg[alt="diagram.",align="center"]

* For this controller will also be used
. *Gitlab Facade* from Interfaces to use their attributes or properties (GetBranchesList, GetTree)
. *JsonSerializer* to deserialize the json that the get returned



